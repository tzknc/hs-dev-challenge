import "./App.css";
import Panel from "./Panel";
import { useState } from "react";
import SearchBox from "./components/SearchBox";
import CloseBox from "./components/CloseBox";
import Menu from "./components/Menu";
import { CSSTransition } from 'react-transition-group';
import {
  ApolloClient,
  InMemoryCache,
  gql
} from "@apollo/client";

const client = new ApolloClient({
  uri: 'https://spotify-graphql-server.herokuapp.com/graphql',
  cache: new InMemoryCache()
});

let searchTimeout = null;

function App() {
  // state
  const [albums, setAlbums] = useState([]);
  const [resultAlbums, setResultAlbums] = useState([]);
  const [animate, setAnimate] = useState(false);
  const [isSingle, setSingle] = useState(false);
  const [selectedId, setSelectedId] = useState('');

  // update the results
  function changeSrchString(evt) {

    const search = evt.target.value
    
    if(search === '') {return}
    // slow down, avoid million requests to API.
    if (searchTimeout !== null ) {
      clearTimeout(searchTimeout)
    }
    searchTimeout = setTimeout( () => {
      fetchAlbums(search)
    },300 )
  }

  function fetchAlbums(search) {
    client
    .query({
        query: gql`
        {
          queryArtists(byName:"${search}") {
            name
            id
            image
            albums {
              name 
              id
              image
            }
          }
        }
        `
      }).then((result) => {
        parseAlbums(result);
        setAnimate(false);
        return result
      });
  }
  // this is duplicated, ... 
  function updateAlbumPick(selectedId) {
    // setSingle(true);
    setAnimate(false);
    setSelectedId(selectedId)
    // find the corrct album
    const chosenAlbum = resultAlbums.filter(function(item) { 
      return item.id === selectedId;
    });
    setTimeout( () => {
      setAlbums(chosenAlbum);
    },250 )
  }

  function pickAlbums(selectedId) {
    if(isSingle) {return}

    setSingle(true);
    setAnimate(false);
    setSelectedId(selectedId)
    // find the corrct album
    const chosenAlbum = resultAlbums.filter(function(item) { 
      return item.id === selectedId;
    });
    setTimeout( () => {
      setAlbums(chosenAlbum);
    },250 )
  }

  function closeAlbum() {
    setSingle(false);
    setAnimate(false)
    setTimeout( () => {
      setAlbums(resultAlbums);
    },250 )
  }
  
  function parseAlbums(results) {

    if(!results.data.queryArtists) {return}
    setSingle(false);

    let allAlbums = results.data.queryArtists[0].albums
    const artistName = results.data.queryArtists[0].name
    let albums = [];
    if(allAlbums.length > 3) {
      allAlbums = allAlbums.slice(0,3);
    }

    for (const album of allAlbums) {
      albums.push({
        name: album.name,
        image: album.image,
        id: album.id,
        artist: artistName
      })
    }   
    setTimeout( () => {
      setAlbums(albums);
      setResultAlbums(albums);
      setAnimate(true)
    },400 )
  }


  return (
    <div className="bg-black">
      <div>
        <SearchBox 
          onSearchStringChange={changeSrchString}
        />
        {isSingle && (
        <CloseBox 
          onClicked={closeAlbum}
        />)}
      </div>
      <CSSTransition
        in={animate}
        timeout={950}
        classNames="update"
        onExited={() => setAnimate(true)}
      >
      <div className="wrapper">
        {albums.map((album,index) => (
            <Panel
              albumName={album.name}
              albumImage={album.image}
              albumArtist={album.artist}
              key={index}
              albumId={album.id}
              onClicked={pickAlbums}
            />
          ))}
      </div>
      </CSSTransition>
          <div className={`${isSingle ? "show" : "hide"} menu-bottom text-red-500 absolute left-2 bottom-2`}>
            {resultAlbums.map((album,index) => (
                <Menu
                  albumId={album.id}
                  albumName={album.name}
                  key={index}
                  onClicked={updateAlbumPick}
                />
              ))}
          </div>
    </div>
  );
}

export default App;
