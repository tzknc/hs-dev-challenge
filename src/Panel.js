import React from "react";

const Panel = ({ albumName, albumArtist, albumImage, albumId, onClicked}) => {

return (
  <div
  onClick={() => onClicked(albumId)}
  style={{ backgroundImage: `url(${albumImage})`}}
  className="wrapper__panel bg-cover bg-center transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-105">
    <div className="backgroundBox rounded bg-black bg-opacity-60">
      <h2 className="p-2 mt-8 text-center text-6xl text-gray-300">{albumName}</h2>
      <h3 className="mb-8 text-center text-3xl text-gray-400" >{albumArtist}</h3>
      <h5 className="mb-8 text-center text-xs text-gray-600" >{albumId}</h5>
    </div>
  </div>
)}

export default Panel;
