import React from "react";

export default function Searchbox(props) {
  return (
    <div className="searchbox ">
      <div className="flex justify-center">
        <div className="m-8">
          <input
            type="text"
            name="name"
            className="
              border-b-2 px-3 py-3 placeholder-gray-400 text-gray-50 relative bg-gray-300 bg-opacity-0 
              text-center rounded text-5xl border-gray-400 outline-none 
              focus:outline-none focus:text-red-600 focus:border-red-600"
            onChange={props.onSearchStringChange}
            placeholder="type here"
            autoComplete="hidden"
          />
        </div>
      </div>
    </div>
  );
}
