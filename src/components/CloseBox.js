import { ArrowLeftIcon } from '@heroicons/react/outline';

export default function CloseBox(props) {
  
  return (
    <div className="z-10 closebox absolute top-10 right-10 text-red transition duration-500 ease-in-out transform hover:scale-125" 
    onClick={props.onClicked}
    >
      <ArrowLeftIcon className="h-24 w-24 text-gray-500 transition hover:text-gray-300"/>
    </div>
  );
}


