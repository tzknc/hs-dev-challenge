import { ArrowLeftIcon } from '@heroicons/react/outline';

export default function Menu({albumId, albumName, onClicked}) {

  return (
    <div  
      className="text-md m-3 transition duration-200 ease-in-out transform hover:scale-105">
         <ArrowLeftIcon className="h-6 w-6 text-gray-200 transition hover:text-gray-300 inline"/>
      <span
        className=" p-2 bg-white transition duration-200 transform hover:bg-black"
        onClick={() => onClicked(albumId)}
      >  {albumName} </span>
    </div>
  );
}
