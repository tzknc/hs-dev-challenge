# hs-interactive-developer-coding-challenge

## Specification

Hello!

In order for us to see a bit of how you work and a bit of what you know we came up with this technical challenge. Here you're going to consume from a publicly accessible Graphql API, and display some of its information in a interactive way.

We're are going to be using this [unofficial Spotify API](https://spotify-graphql-server.herokuapp.com/graphql) for that.

This repository is a skeleton application you can use as starting point (but if you feel like deleting all the code and starting from scratch nobody is going to stop you). It should work out of the box with only `yarn` and `yarn start` (or with `npm` if that's your cup of tea).

The requirements for this project are found below. Please read them carefully. We usually try to avoid any tricks or traps in this type of test so if something is not clear please don't hesitate to contact us.

## The task

- There's a search bar on top in which the user might input the name of a band
- When a band is found then the top 3 albuns are displayed on 3 vertical fullscreen panes each one occupying about 1/3 of the screen width:
  - The background of each pane must be the image of the album
  - The title of the corresponding album must be displayed in the center of the pane
  - When a user hover a pane that pane must be highlighted. **There must be an animated transition here**.
  - When a user clicks on a pane it should expand and take entire screen. **There must be an animated transition here**.
  - When a pane is expanded the background should take the entire screen but keep its proportions (don't worry about image quality :wink:)
  - When a pane is expanded the titles of all three panes must become a navigation at the bottom of the screen. **There must be an animated transition here**.
  - When a pane is expanded there must be a close button on one of the top corners that restores the page to the initial state (i.e.: three panes taking about 1/3 of the screen width each)

**Below is an example of our take on that interaction**

![Reference](./reference.png)

[Live](https://www.highsnobiety.com/p/ralph-lauren-sports-history/)

- The animations and transitions don't have to be the same as in the reference. Get creative :wink:

- The search must be made against this [unofficial Spotify GraphQL API](https://spotify-graphql-server.herokuapp.com/graphql). Here's an example of a query:

  ```
    {
      queryArtists(byName:"Queen") {
        name
        id
        image
        albums {
          name 
          id
          image
        }
      }
    }
  ```

<br />
<br />

- You're not required to provide a solution for other use paths other the the ideal one. So, for instance, if no band is found or the band has less than 3 albuns, then it's OK to display nothing.
- You're not required to provide a responsive solution. If the interaction works for desktop that's good enough.

## Requirements

- You must use **React**. This is the main library in our frontend currently.
- You must use **GraphQL**. This is the main form our frontend retrieves data currently.
- For the interactions and animations you can use any library or technique you want, not necessarily React.

## How to Deliver

- Fork this repository

- When you're done push your changes and send us the link to your fork (don't worry, this is a private repo, noone but us is gonna know)
  - If you feel more comfortable, you can deliver it on another source code hosting (like Gitlab), send us your solution via e-mail in a compacted file or as a link to compacted file in a storage service (like Google Drive, Dropbox, AWS S3).

- Please attach to this README.md any considerations you have or any instructions to setup and run your code if it differs from `yarn/npm install` & `yarn/npm start`

- Please feel free to reach out if something is not clear or you have any questions. We will be glad to interact with you and answer any question you may have.

## Tips & Tricks

- If it's taking you more than 4-5 hours to complete this challenge, you're probably overcomplicating something. Keep it simple, clean and smart.

- Feel free to install any libraries you think may be necessary, but remember it's not such a complicated project so try to avoid things that could be easily solved with vanilla JS. E.g.:
  - lodash :thumbsup:
  - [leftpad](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code/) :thumbsdown:

- Commit frequently. If you want you can adopt some commit convention like [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

- We'll have time to discuss a bit more about this project in a later moment, so take note of any improvements you'd make if this was a real-life project and, if any, the corners you had to cut.

<br />
<br />
<br />
<br />

<p style="text-align: center;">Good Luck!</p>
<p style="text-align: center;">We're looking forward to see what you can do!</p>


